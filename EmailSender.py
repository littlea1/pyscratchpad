# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

COMMASPACE = ","


class EmailSender():
    """
    Utility class to send simple text/html emails.
    """

    def __init__(self, server, port, user, password, tls):
        """
        Constructor

        :param server: Name or IP address of smtp server
        :param sender: Email address of the sender
        :param recipients: List of recipients' email addresses
        :param subject: Subject of the email
        :return: An instance of the class
        """
        self.user = user
        self.password = password
        self.port = port
        self.tls = tls
        self.server = server

    def send_email_as_text(self, sender, recipients, subject, body):
        """

        :param body: body of the email
        :return: Nothing
        """
        to_part = COMMASPACE.join(recipients)

        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = sender
        msg['To'] = to_part
        # Record the MIME types of both parts - text/plain and text/html.
        part = MIMEText(body, 'plain')

        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this case
        # the HTML message, is best and preferred.
        msg.attach(part)
        self._send_email(sender, recipients, msg)

    def _send_email(self, sender, recipients, msg):
        """
        Send the email

        :return: nothing
        """
        # Send the message via local SMTP server.
        server = smtplib.SMTP(self.server, self.port)
        server.ehlo()
        if self.tls:
            server.starttls()
        server.login(self.user, self.password)
        # sendmail function takes 3 arguments: sender's address, recipient's address
        # and message to send - here it is sent as one string.
        server.sendmail(sender, recipients, msg.as_string())
        server.quit()





