from selenium import webdriver
from EmailSender import EmailSender

#'https://www.google.com/flights/#search;f=EWR;t=MIA;d=2016-12-27;r=2017-01-02;s=0'
GOOGLE_FLIGHTS_URL = 'https://www.google.com/flights/#search;f={};t={};d={};r={};s=0'


# Opens Firefox
browser = webdriver.Firefox()
# make it wait up to XX seconds for the load
browser.implicitly_wait(10)

browser.get('https://www.google.com/flights/#search;f=EWR;t=MIA;d=2016-12-27;r=2017-01-02;s=0')
root_id = browser.find_element_by_id("root")
root_str = root_id.get_attribute('class').split("-")[0]
prices = [e.text for e in browser.find_elements_by_class_name("{}-d-zb".format(root_str))] # CNAVQLC
airline = [e.text for e in browser.find_elements_by_class_name("{}-d-j".format(root_str))]
hours = [e.text for e in browser.find_elements_by_class_name("{}-d-ac".format(root_str))]
for i in xrange(len(prices)):
    print prices[i], airline[i], hours[i]

browser.quit()

def main():
    """Main entry point"""
    pass


if __name__ == "__main__":
    main()