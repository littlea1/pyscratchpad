{
  "account": {
    "1": "FundA",
    "2": "FundA",
    "3": "FundA",
    "4": "FundA",
    "5": "FundA",
    "6": "FundA",
    "7": "FundA",
    "8": "FundA",
    "9": "FundA",
    "10": "FundA"
  },
  "amount": {
    "1": 100,
    "2": 150,
    "3": -300,
    "4": 400,
    "5": -100,
    "6": 200,
    "7": 500,
    "8": -1000,
    "9": 100,
    "10": -50
  },
  "transaction_date": {
    "1": "2019-01-01",
    "2": "2019-01-02",
    "3": "2019-01-03",
    "4": "2019-02-01",
    "5": "2019-02-05",
    "6": "2019-02-10",
    "7": "2019-03-01",
    "8": "2019-03-05",
    "9": "2019-03-18",
    "10": "2019-02-05"
  }
}