#!/bin/python
import requests
import bs4
from EmailSender import EmailSender
import os

__author__ = "Alain Ledon"

"""
Scrape the results of a Velogames Fantasy Cycling league
"""

LEAGUE_URL = r"http://www.velogames.com/giro-ditalia/2016/leaguescores.php?league=2013829"
EMAIL_SUBJECT = "Giro d'Italia 2016 Velogames Pork and Cigars League Standings"

EMAIL_HOST = "smtp.webfaction.com"
EMAIL_PORT = 587
EMAIL_USER = os.environ['WEBFACTION_EMAIL_USR']
EMAIL_PASSWORD = os.environ['WEBFACTION_EMAIL_PWD']
EMAIL_TLS = True
EMAIL_SENDER = "alain@muontec.com"
EMAIL_RECIPIENTS = ["alain.ledon@gmail.com", "aabtzu@gmail.com",
                    "dlosardo@gmail.com", "movsesmus@gmail.com",
                    "aryanab@gmail.com"]


def check_league():
    """
    Check the current active league

    :return: String
    """
    response = requests.get(LEAGUE_URL)
    soup = bs4.BeautifulSoup(response.text, "lxml")

    return_string = "Standings\n"

    all_users_records = [x.string for x in soup.select("p.born")]
    for i in xrange(0, len(all_users_records), 2):
        return_string = "\n".join([return_string, "{0} = {1}".format(all_users_records[i+1], all_users_records[i])])

    return return_string


def main():
    """
    Main functions

    :return:
    """
    email_text = check_league()
    emailer = EmailSender(EMAIL_HOST, EMAIL_PORT, EMAIL_USER, EMAIL_PASSWORD, EMAIL_TLS)
    emailer.send_email_as_text(EMAIL_SENDER, EMAIL_RECIPIENTS, EMAIL_SUBJECT, email_text)

    print email_text

if __name__ == "__main__":
    main()